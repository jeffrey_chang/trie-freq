#include "DictionaryTrie.hpp"
// #define debug(...) printf(__VA_ARGS__)
#define debug(...)

node::~node(){
    for (node *child : children)
        delete child;
}

unsigned int char_to_int(char c) {
    if (c == ' ')
        return N_CHARS - 1;
    return c - 'a';
}

char int_to_char(unsigned int n) {
    if (n == N_CHARS - 1)
        return ' ';
    else
        return 'a' + n;
}

/* Create a new Dictionary that uses a Trie back end */
DictionaryTrie::DictionaryTrie(){}

/* Insert a word with its frequency into the dictionary.
 * Return true if the word was inserted, and false if it
 * was not (i.e. it was already in the dictionary or it was
 * invalid (empty string) */
bool DictionaryTrie::insert(std::string word, unsigned int freq)
{
    debug("inserting word %s with frequency %u\n", word.c_str(), freq);

    if (find(word))
        return false;
    if (word.length() == 0)
        return false;

    node *curr = &head;
    debug("curr at head, at %p\n", curr);
    for (char c : word) {
        unsigned int index = char_to_int(c);
        if (curr->children[index] == NULL) {
            curr->children[index] = new node;
            curr->children[index]->parent = curr;
        }
        if (freq > curr->top_freq)
            curr->top_freq = freq;

        debug("moving curr to %c, at %p\n", c, curr->children[index]);
        curr = curr->children[index];
    }

    if (freq > curr->top_freq)
        curr->top_freq = freq;
    curr->leaf = true;
    curr->freq = freq;
    debug("word %s's leaf is at %p, with freq = %u\n\n", word.c_str(), curr, freq);
    return true;
}

/* Return true if word is in the dictionary, and false otherwise */
bool DictionaryTrie::find(std::string word) const
{
    debug("finding word %s\n", word.c_str());

    const node *curr = &head;
    for (char c : word) {
        unsigned int index = char_to_int(c);
        if (curr->children[index] == NULL)
            return false;
        curr = curr->children[index];
    }

    if (!curr->leaf)
        return false;

    return true;
}

/* Return up to num_completions of the most frequent completions
 * of the prefix, such that the completions are words in the dictionary.
 * These completions should be listed from most frequent to least.
 * If there are fewer than num_completions legal completions, this
 * function returns a vector with as many completions as possible.
 * If no completions exist, then the function returns a vector of size 0.
 * The prefix itself might be included in the returned words if the prefix
 * is a word (and is among the num_completions most frequent completions
 * of the prefix)
 */
std::vector<std::string> DictionaryTrie::predictCompletions(std::string prefix, unsigned int num_completions)
{
    std::vector<std::string> words;
    std::vector<unsigned int> freqs;

    // Find the prefix 
    debug("\nfinding prefix %s\n", prefix.c_str());

    node *curr = &head;
    debug("curr at head, at %p\n", curr);
    for (char c : prefix) {
        unsigned int index = char_to_int(c);
        debug("moving curr to %c, at %p\n", c, curr->children[index]);
        curr = curr->children[index];
    }
    debug("prefix found.\n\n");
    node *prefix_location = curr;

    for (unsigned int i = 0; i < num_completions; i++) {
        std::string word = prefix;
        curr = prefix_location;

        if (curr->top_freq == 0) // no completions exist
            break; 

        // Find most frequent word and set its frequency to zero 
        while (curr->freq != curr->top_freq) {
            debug("curr at %p; curr->freq = %u, curr->top_freq = %u\n",
                    curr, curr->freq, curr->top_freq);
            for (unsigned int j = 0; j < N_CHARS; j++) {
                node *child = curr->children[j];
                if (child != NULL && child->top_freq == curr->top_freq) {
                    word += int_to_char(j);

                    debug("moving curr to %p (letter %c)\n", child, int_to_char(j));
                    curr = child;
                    break;
                }
            }
        }

        debug("%s is the %u'th most frequent word.\n", word.c_str(), i + 1);

        // Temporarily remove word from trie 
        debug("removing %s\n", word.c_str());
        words.push_back(word);
        freqs.push_back(curr->freq);
        curr->freq = 0;
        curr->leaf = false;

        // Go back up and update each node's top_freq on the way 
        while (curr != NULL) {
            debug("updating top_freq at %p\n", curr);
            curr->top_freq = curr->freq;
            for (node *child : curr->children) {
                if (child != NULL && child->top_freq > curr->top_freq)
                    curr->top_freq = child->top_freq;
            }
            curr = curr->parent;
        }
        debug("\n");
    }

    // Put back the words we took out 
    for (unsigned int i = 0; i < words.size(); i++) {
        debug("putting back %s\n", words[i].c_str());
        insert(words[i], freqs[i]);
    }

    return words;
}

/* Destructor */
DictionaryTrie::~DictionaryTrie(){}


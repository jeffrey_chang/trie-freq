#include "DictionaryTrie.cpp"
#include <cstdio>
#include <vector>
#include <string>
#include <iostream>
#include <cassert>

void test_funcs() {
    assert(int_to_char(0) == 'a');
    assert(char_to_int('a') == 0);
    for(int i = 0; i < N_CHARS; i++) {
        assert(char_to_int(int_to_char(i)) == i);
    }
}

int main() {
    printf("Testing...\n");
    test_funcs();
    DictionaryTrie dict;

    dict.insert("one", 1);
    dict.insert("ones", 2);
    dict.insert("three", 3);
    dict.insert("thirty", 30);
    dict.insert("thirty three", 33);

    std::cout << std::endl;
    std::vector<std::string> ss = dict.predictCompletions("th", 5);
    for (auto s : ss)
        std::cout << s << std::endl;

    return 0;
}
